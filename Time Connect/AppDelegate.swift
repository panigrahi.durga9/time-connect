//
//  AppDelegate.swift
//  Time Connect
//
//  Created by Mona on 22/11/20.
//  Copyright © 2020 TimeConnect. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var defaults = UserDefaults.standard

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .black
        IQKeyboardManager.shared.enable = true
        
        if UserDefaults.standard.bool(forKey: "isLogin") {
            let userDetails = UserSessionManager.shared.loginDetails
            let userDetails1 = UserSessionManager.shared.loginDetailss
            let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
            self.window?.rootViewController = destination
            destination.loginData = userDetails
            destination.adminloginModel = userDetails1
            destination.modalPresentationStyle = .custom
            
        }else if UserDefaults.standard.bool(forKey: "isLogin"){
            let userDetails = UserSessionManager.shared.loginDetailss
            let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsviewController") as! SettingsviewController
            self.window?.rootViewController = destination
            destination.adminloginData = userDetails
            destination.modalPresentationStyle = .custom
        }
        else{
           let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
           self.window?.rootViewController = destination
           destination.modalPresentationStyle = .custom
        }
        
        self.window?.makeKeyAndVisible()
        return true
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //print("Call update data method")
        //updateData()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}



extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
