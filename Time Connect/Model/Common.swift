//
//  Common.swift
//  PaxChat
//
//  Created by chhavi  kaushik on 16/10/17.
//  Copyright © 2017 iquincesoft. All rights reserved.
//

import UIKit
import Foundation

class Common: NSObject {

    // Declare class instance property
    static let sharedInstance = Common()
    
    // Declare an initializer
    override init() {
        print("Singleton class Chalu")
        
    }

    //adding corner to any button
    func addBtnBorder(aButton : UIButton , cornerValue : CGFloat = 5) {
        aButton.layer.cornerRadius = cornerValue
    }
    // function for email validation
    func isValidEmail(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
    func showUserMessage(UserMessaeg : String) {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1
        
        let alert2 = UIAlertController(title: "TIME CONNECT", message: UserMessaeg, preferredStyle: .alert)
        let defaultAction2 = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert2.addAction(defaultAction2)
        
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert2, animated: true, completion: nil)
    }
    
    func isFieldBlank(textfield : String) -> Bool {
        return textfield.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    func isValidPassword(password : String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
            if(regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, password.count)) != nil){

                if(password.count>=2 && password.count<=20){
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        } catch {
            return false
        }
    }
    
    func isValidPhoneNumber(phNo : String) -> Bool{
        let regex = try! NSRegularExpression(pattern: "^[0-9]\\d{9}$", options: .caseInsensitive)
        let valid = regex.firstMatch(in: phNo, options: [], range: NSRange(location: 0, length: phNo.count)) != nil
        return valid
    }
}
enum ImageDirection{
    case left, right
}
extension UITextField{
    func addButtomBorder(color: CGColor = UIColor.white.cgColor){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - 2, width: self.frame.width, height: 1.0)
        bottomLine.backgroundColor = color
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(bottomLine)
        self.layer.masksToBounds = true
    }
    
    func setTextFieldImage(image: UIImage, direction: ImageDirection = .left){
        let imageView = UIImageView()
        let image = image
        imageView.image = image
        
        if direction == .left{
            self.leftView = imageView
        }
        else{
            self.rightView = imageView;
        }
    }
    
    func setTextFieldImage(image: FAType, direction: ImageDirection = .left){
        let imageView = UIImageView()
        let origImage = UIImage(icon: image, size: CGSize(width: 30, height: 30))
        let tintedImage = origImage.withRenderingMode(.alwaysTemplate)
        imageView.image = tintedImage
        imageView.tintColor = .white
        imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        if direction == .left{
            self.leftView = imageView
            self.leftViewMode = .always
            self.leftView = imageView
        }
        else{
            self.rightView = imageView
            self.rightViewMode = .always
            self.rightView = imageView
        }
    }
    
    func setPlaceholder(placeholder: String, color: UIColor){
        self.attributedPlaceholder = NSAttributedString(string: placeholder,
        attributes: [NSAttributedString.Key.foregroundColor: color])
    }

}
