//
//  Connection.swift
//  VeryVienna
//
//  Created by chhavi  kaushik on 17/10/18.
//  Copyright © 2018 iquincesoft. All rights reserved.
//

import Foundation
import Alamofire

class Connection {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
