//
//  CustomButton.swift
//  Time Connect
//
//  Created by Mona on 26/11/20.
//  Copyright © 2020 TimeConnect. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable open class CustomButton : UIButton {
    
    @IBInspectable var cornorRadius : Double {
        get {
            return Double((self.layer.cornerRadius))
        }
        set{
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderColor1 : UIColor {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set{
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var borderWidth1 : Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set{
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
           get {
               return UIColor(cgColor: self.layer.shadowColor!)
           }
           set {
               self.layer.shadowColor = newValue?.cgColor
           }
    }
       
       /// The opacity of the shadow. Defaults to 0. Specifying a value outside the [0,1] range will give undefined results. Animatable.
       @IBInspectable var shadowOpacity: Float {
           get {
               return self.layer.shadowOpacity
           }
           set {
               self.layer.shadowOpacity = newValue
           }
       }
       
       /// The shadow offset. Defaults to (0, -3). Animatable.
       @IBInspectable var shadowOffset: CGSize {
           get {
               return self.layer.shadowOffset
           }
           set {
               self.layer.shadowOffset = newValue
           }
       }
       
       /// The blur radius used to create the shadow. Defaults to 3. Animatable.
       @IBInspectable var shadowRadius: Double {
           get {
               return Double(self.layer.shadowRadius)
           }
           set {
               self.layer.shadowRadius = CGFloat(newValue)
           }
       }
    
    
}
public extension UIButton
  {

    func alignTextUnderImage(spacing: CGFloat = 6.0)
    {
        if let image = self.imageView?.image
        {
            let imageSize: CGSize = image.size
            self.titleEdgeInsets = UIEdgeInsets(top: spacing, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
            let labelString = NSString(string: self.titleLabel!.text!)
            let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font])
            self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        }
    }
}
