//
//  DetailsCell.swift
//  Time Connect
//
//  Created by Sujit MacBook on 16/12/20.
//  Copyright © 2020 TimeConnect. All rights reserved.
//

import Foundation
import UIKit
class DetailsCell: UITableViewCell{
    
    @IBOutlet weak var switchbutton: UISwitch!
    @IBOutlet weak var detaillabel: UILabel!
}
