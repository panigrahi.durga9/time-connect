//
//  AdminLoginController.swift
//  Time Connect
//
//  Created by Sujit MacBook on 14/12/20.
//  Copyright © 2020 TimeConnect. All rights reserved.
//



import UIKit
import Alamofire
import NVActivityIndicatorView


class AdminLoginController: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var backAdmin: UIButton!
    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var signinButton: UIButton!
    var adminloginModel = [AdminLoginModel]()
    var loginData = [LoginModel]()
    var deviceData = [DeviceDataModel]()
    @IBOutlet weak var signInButton: CustomButton!
    var activityIndicator: NVActivityIndicatorView!

    @IBAction func backbuttonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    fileprivate func setUpView() {
        
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .ballSpinFadeLoader, color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), padding: .zero)
        activityIndicator.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        self.view.addSubview(activityIndicator)
        
        signInButton.cornorRadius = 10
        self.signInButton.clipsToBounds = true
        self.signInButton.backgroundColor = #colorLiteral(red: 0.1646597683, green: 0.3915407658, blue: 0.6790658832, alpha: 1)
        signInButton.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        signInButton.borderWidth1 = 1
        signInButton.shadowColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        signInButton.shadowOffset = CGSize(width: 0, height: 3)
        signInButton.shadowOpacity = 0.6
        signInButton.layer.shadowRadius = 10.0
        self.signInButton.layer.masksToBounds = false
        self.signInButton.setTitleColor(.white, for: .normal)

        
        self.tfUsername.setTextFieldImage(image: FAType.FAUser, direction: .left)
        self.tfUsername.keyboardType = .emailAddress
        self.tfPassword.setTextFieldImage(image: FAType.FALock, direction: .left)
        self.tfPassword.isSecureTextEntry = true
        
        self.tfUsername.setPlaceholder(placeholder: "Enter Your Mail / Phone No.", color: UIColor.white.withAlphaComponent(0.6))
        self.tfPassword.setPlaceholder(placeholder: "Enter Your Password", color: UIColor.white.withAlphaComponent(0.6))
        
        self.tfUsername.textColor = .white
        self.tfPassword.textColor = .white
        
       
    }
//    func getDevice(){
//
//    }
    override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
           self.tfUsername.addButtomBorder()
           self.tfPassword.addButtomBorder()
       }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        if loginData.count > 0{
        let apiUrl = "https://api.timeconnect.net/data/getdevice?key=GREIyYPzs7eq0e&salt=sIgTZQWmtChan6XifU1CFd"
        let params =  "&staff_id=" + "\(self.loginData[0].staff_id)" + "&admin_id=" + "\(self.loginData[0].admin_id)"
        let url = String(apiUrl + params)
        getDevice(url)
        }
        tfUsername.alpha = 0
        tfPassword.alpha = 0
        signInButton.alpha = 0
        
        UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .transitionCurlDown, animations: {
            self.tfUsername.alpha = 1
        }) { _ in
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
                self.tfPassword.alpha = 1
                
            })

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
                    self.signInButton.alpha = 1
                    
                })
            }
        }
    }
    func getDevice(_ url: String){
         if Connection.isConnectedToInternet(){
          self.activityIndicator.startAnimating()
         Alamofire.request(url,
                           method: .get,
                           parameters: nil)
                 .validate().responseJSON { (response) in
                     guard response.result.isSuccess else {
                         self.activityIndicator.stopAnimating()
                         return
                     }
                     
                     let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                     self.activityIndicator.stopAnimating()
                     let results = resposeJSON["status"] as? Bool ?? false
                     
                     if results {
                         UserDefaults.standard.set(true, forKey: "isLogin")
                         let data = resposeJSON["data"] as? NSArray ?? NSArray()
                         let adminloginData = data[0] as? NSDictionary ?? NSDictionary()
                         let enable_scanner = adminloginData["enable_scanner"] as? String ?? ""
                         let rear_camera = adminloginData["rear_camera"] as? String ?? ""
                         let device_sleep = adminloginData["device_sleep"] as? String ?? ""
                        
                         
                         let deviceDataModel = DeviceDataModel(enable_scanner: enable_scanner, rear_camera: rear_camera, device_sleep: device_sleep)
                         
                         self.deviceData.append(deviceDataModel)
                         // save it easily!
                         //UserSessionManager.shared.loginDetails = self.adminloginModel
                         //self.goToCheckInPage()
                         
                     }else{
                         self.showAlertwhileApiFails(resposeJSON)
                     }
             }
         }
    }
    // MARK: - Ibaction buttons

    
    @IBAction func btnLoginPressed(_ sender: CustomButton) {
        
        if Common.sharedInstance.isFieldBlank(textfield: tfUsername.text ?? "") || Common.sharedInstance.isFieldBlank(textfield: tfPassword.text ?? "") {
            
            Common.sharedInstance.showUserMessage(UserMessaeg: "Email/Password field should not be blank")
        }
        else if !Common.sharedInstance.isValidPassword(password: tfPassword.text ?? ""){
            Common.sharedInstance.showUserMessage(UserMessaeg:"Please enter valid Password !")
        }else if Common.sharedInstance.isValidEmail(email: tfUsername.text ?? "") || Common.sharedInstance.isValidPhoneNumber(phNo: tfUsername.text ?? ""){
            
            //https://api.terraapp.net/data/GetStaffLogin?key=GREIyYPzs7eq0e&salt=sIgTZQWmtChan6XifU1CFd&email_id=durga@muvi.com&password=123456
            let apiUrl = "https://api.timeconnect.net/data/GetAdminLogin?key=GREIyYPzs7eq0e&salt=sIgTZQWmtChan6XifU1CFd"
            let params =  "&email_id=" + "\(tfUsername.text ?? "")" + "&password=" + "\(tfPassword.text ?? "")"
            let url = String(apiUrl + params)
            //if UserDefaults.standard.string(forKey: "userType") == "Admin"{
            CallLoginApi(url)
//            }else{
//               let alertController = UIAlertController(title: "Alert!", message: "You are not Admin", preferredStyle: .alert)
//               let okAction = UIAlertAction(title: "Ok", style: .cancel) { (_) in
//                   
//               }
//               alertController.addAction(okAction)
//               self.present(alertController, animated: true, completion: nil)
//            }
        }else{
              Common.sharedInstance.showUserMessage(UserMessaeg:"Please enter valid Email / Phone number!")
        }
     
    }
    
    //MARK: - TextField Delegaate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 2 {
           tfPassword.isSecureTextEntry = true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        return true;
    }
   
    fileprivate func goToCheckInPage() {
        
             let destination = self.storyboard?.instantiateViewController(withIdentifier: "SettingsviewController") as! SettingsviewController
             destination.adminloginData = self.adminloginModel
             destination.loginData = self.loginData
             destination.deviceData = self.deviceData
             destination.modalPresentationStyle = .custom
             self.present(destination, animated: true, completion: nil)
        
        
       
    }
    
    fileprivate func showAlertwhileApiFails(_ resposeJSON: NSDictionary) {
        let alertController = UIAlertController(title: "Alert!", message: resposeJSON["message"] as? String ?? "Staff not Matching", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel) { (_) in
            
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func CallLoginApi(_ url: String)  {
       
        if Connection.isConnectedToInternet(){
         self.activityIndicator.startAnimating()
        Alamofire.request(url,
                          method: .get,
                          parameters: nil)
                .validate().responseJSON { (response) in
                    guard response.result.isSuccess else {
                        self.activityIndicator.stopAnimating()
                        return
                    }
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    let results = resposeJSON["status"] as? Bool ?? false
                    
                    if results {
                        UserDefaults.standard.set(true, forKey: "isLogin")
                        let data = resposeJSON["data"] as? NSArray ?? NSArray()
                        let adminloginData = data[0] as? NSDictionary ?? NSDictionary()
                        let user_name = adminloginData["user_name"] as? String ?? ""
                        let user_id = adminloginData["user_id"] as? String ?? ""
                        let user_email = adminloginData["user_email"] as? String ?? ""
                        let company_name = adminloginData["company_name"] as? String ?? ""
                        let enable_scanner = adminloginData["enable_scanner"] as? String ?? ""
                        let rear_camera = adminloginData["rear_camera"] as? String ?? ""
                        let device_sleep = adminloginData["device_sleep"] as? String ?? ""
                        
                        let loginModel = AdminLoginModel(user_name: user_name, user_id:user_id, user_email: user_email, company_name: company_name, enable_scanner: enable_scanner, rear_camera: rear_camera, device_sleep: device_sleep)
                        
                        self.adminloginModel.append(loginModel)
                        // save it easily!
                        UserSessionManager.shared.loginDetailss = self.adminloginModel
                        self.goToCheckInPage()
                        
                    }else{
                        self.showAlertwhileApiFails(resposeJSON)
                    }
            }
        }
        
    }
    
}
struct AdminLoginModel : Codable {
    var user_name = ""
    var user_id = ""
    var user_email = ""
    var company_name = ""
    var enable_scanner = ""
    var rear_camera = ""
    var device_sleep = ""
}




