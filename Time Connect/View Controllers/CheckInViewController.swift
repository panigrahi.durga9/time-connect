//
//  CheckInViewController.swift
//  TERRA
//
//  Created by Mona on 19/11/20.
//  Copyright © 2020 iquincesoft. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import CoreLocation
import AudioToolbox

class CheckInViewController: UIViewController {

    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var btnScanQRCode: UIButton!
    @IBOutlet weak var btnManuaalyEnterPin: UIButton!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var lineView: UIView!
    var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var titleOfPage: UILabel!
    @IBOutlet weak var logOutButton: CustomButton!
    @IBOutlet weak var signInButton: CustomButton!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var checkInTimeLabel: UILabel!
    @IBOutlet weak var checkOutTimeLabel: UILabel!
    
    
    
    
    
    var locationManager = CLLocationManager()
    
    var longitude   = ""
    var latitude    = ""
    var currentTime = ""
    var city        = ""
    var country     = ""
    var currnetDate = ""

    
    var loginData = [LoginModel]()
    var staffId = ""
    var adminId = ""
    var adminloginModel = [AdminLoginModel]()
    var deviceData = [DeviceDataModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .ballSpinFadeLoader, color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), padding: .zero)
        activityIndicator.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        self.view.addSubview(activityIndicator)

        // Do any additional setup after loading the view.
        Common.sharedInstance.addBtnBorder(aButton: btnScanQRCode)
        Common.sharedInstance.addBtnBorder(aButton: btnManuaalyEnterPin)
//        staffId = UserSessionManager.shared.loginDetails[0].staff_id
//        adminId = UserSessionManager.shared.loginDetails[0].admin_id
        
        
        setUpButtons()
        
        self.dateView.backgroundColor = #colorLiteral(red: 0.08810380846, green: 0.340724349, blue: 0.5595758557, alpha: 1)
        self.dateView.layer.cornerRadius = 10.0
        self.dateView.layer.borderWidth = 1
        self.dateView.layer.borderColor = UIColor.white.cgColor
        if UserDefaults.standard.bool(forKey: "isLogin") && UserDefaults.standard.string(forKey: "userType") == "Staff"{
            
            self.titleOfPage.text = self.loginData[0].staff_name 
        }else if UserDefaults.standard.bool(forKey: "isLogin") && UserDefaults.standard.string(forKey: "userType") == "Admin"{
            self.titleOfPage.text = self.adminloginModel[0].user_name
        }
        //self.checkInTimeLabel.isHidden = true
        self.checkOutTimeLabel.isHidden = true
        self.logOutButton.addTarget(self, action: #selector(logOutAction), for: .touchUpInside)
        self.signInButton.addTarget(self, action: #selector(signin), for: .touchUpInside)
        
    }
    
    
    
    func getDevice(_ url: String){
        self.deviceData.removeAll()
         if Connection.isConnectedToInternet(){
          self.activityIndicator.startAnimating()
         Alamofire.request(url,
                           method: .get,
                           parameters: nil)
                 .validate().responseJSON { (response) in
                     guard response.result.isSuccess else {
                         self.activityIndicator.stopAnimating()
                         return
                     }
                     
                     let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                     self.activityIndicator.stopAnimating()
                     let results = resposeJSON["status"] as? Bool ?? false
                     
                     if results {
                         UserDefaults.standard.set(true, forKey: "isLogin")
                         let data = resposeJSON["data"] as? NSArray ?? NSArray()
                         let adminloginData = data[0] as? NSDictionary ?? NSDictionary()
                         let enable_scanner = adminloginData["enable_scanner"] as? String ?? ""
                         let rear_camera = adminloginData["rear_camera"] as? String ?? ""
                         let device_sleep = adminloginData["device_sleep"] as? String ?? ""
                        
                         
                         let deviceDataModel = DeviceDataModel(enable_scanner: enable_scanner, rear_camera: rear_camera, device_sleep: device_sleep)
                         
                         self.deviceData.append(deviceDataModel)
                         // save it easily!
                         //UserSessionManager.shared.loginDetails = self.adminloginModel
                         //self.goToCheckInPage()
                        self.animateButtons()
                         
                     }else{
                         self.showAlertwhileApiFails(resposeJSON)
                     }
             }
         }
    }
     fileprivate func showAlertwhileApiFails(_ resposeJSON: NSDictionary){
        let alertController = UIAlertController(title: "Alert!", message: resposeJSON["message"] as? String ?? "Staff not Found", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel) { (_) in
            
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    fileprivate func setUpButtons() {
        btnScanQRCode.addTarget(self, action: #selector(goToScannerPage), for: .touchUpInside)
        btnManuaalyEnterPin.addTarget(self, action: #selector(goToPinPage), for: .touchUpInside)
        let QRimage = UIImage(icon: .FAQrcode
            , size: CGSize(width: 60, height: 60), orientation: .down, textColor: .white, backgroundColor: .clear)
        btnScanQRCode.setImage(QRimage, for: .normal)
        btnScanQRCode.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 5)
        
        let image = UIImage(icon: .FAKeyboardO, size: CGSize(width: 60, height: 60), orientation: .down, textColor: .white, backgroundColor: .clear)
        btnManuaalyEnterPin.setImage(image, for: .normal)
        btnManuaalyEnterPin.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 5)
        
        btnScanQRCode.backgroundColor = #colorLiteral(red: 0.08810380846, green: 0.340724349, blue: 0.5595758557, alpha: 1)
        btnManuaalyEnterPin.backgroundColor = #colorLiteral(red: 0.08810380846, green: 0.340724349, blue: 0.5595758557, alpha: 1)
        
        btnScanQRCode.layer.cornerRadius = 8.0
        btnManuaalyEnterPin.layer.cornerRadius = 8.0
        
        self.btnScanQRCode.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.btnScanQRCode.layer.borderWidth = 1
        
        self.btnManuaalyEnterPin.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.btnManuaalyEnterPin.layer.borderWidth = 1
        
        self.btnScanQRCode.alignTextUnderImage()
        self.btnManuaalyEnterPin.alignTextUnderImage()
    }
    
    fileprivate func animateButtons() {
        btnScanQRCode.alpha = 0
        btnManuaalyEnterPin.alpha = 0
        logo.alpha = 0
        lineView.alpha = 0
//        if UserDefaults.standard.string(forKey: "userType") == "Admin"{
//            if adminloginModel.count > 0{
//                self.stackview.isHidden = false
//                if self.adminloginModel[0].enable_scanner == "1"{
//                    lineView.isHidden = false
//                    btnScanQRCode.isHidden = false
//
//
//                }
//                else{
//                    lineView.isHidden = true
//                    btnScanQRCode.isHidden = true
//
//                }
//                if self.adminloginModel[0].device_sleep == "1"{
//                    UIApplication.shared.isIdleTimerDisabled = false
//                }else{
//                    UIApplication.shared.isIdleTimerDisabled = true
//                }
//            }else{
//                self.stackview.isHidden = true
//            }
//        }
       // else{
            if self.deviceData.count > 0{
            
            self.stackview.isHidden = false
            if self.deviceData[0].enable_scanner == "1"{
                lineView.isHidden = false
                btnScanQRCode.isHidden = false
                
                
            }
            else{
                lineView.isHidden = true
                btnScanQRCode.isHidden = true
                
            }
            if self.deviceData[0].device_sleep == "1"{
                UIApplication.shared.isIdleTimerDisabled = false
            }else{
                UIApplication.shared.isIdleTimerDisabled = true
            }
        }else{
            self.stackview.isHidden = true
        }
        //}
        UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 0, initialSpringVelocity: 0, options: .transitionCurlDown, animations: {
            self.logo.alpha = 1
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 0, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.btnScanQRCode.alpha = 1
                
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 0, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                    self.lineView.alpha = 1
                    
                })
        }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 0, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                    self.btnManuaalyEnterPin.alpha = 1
                    
                })
            }
        }
        
    }

    fileprivate func calculationForDateAndDay() {
        var timeZone = TimeZone.current
        timeZone = TimeZone(identifier: "America/Los_Angeles")!
        timeZone = TimeZone(abbreviation: "PDT")!
        
        let date = Date()
        let df = DateFormatter()
        df.timeZone = timeZone
        df.dateFormat = "yyyy-MM-dd"
        self.currnetDate = df.string(from: date)
        
        let time = DateFormatter()
        time.dateFormat = "HH:mm:ss"
        time.timeZone = timeZone
        self.currentTime = time.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = timeZone
        timeLable.text = dateFormatter.string(from: date)
        
        let year = DateFormatter()
        year.dateFormat = "EEEE, MMM d, yyyy"
        year.timeZone = timeZone
        dayLabel.text = year.string(from: date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
       // if UserDefaults.standard.string(forKey: "userType") == "Admin"{
        // self.animateButtons()
            
        
            if UserDefaults.standard.string(forKey: "userType") == "Admin"{
               // self.staffId = self.loginData[0].staff_id
                self.adminId = self.adminloginModel[0].user_id
            }else{
               // self.staffId = self.loginData[0].staff_id
                if self.loginData.count > 0{
                self.adminId = self.loginData[0].admin_id
            }
        }
       
        
        let apiUrl = "https://api.timeconnect.net/data/getdevice?key=GREIyYPzs7eq0e&salt=sIgTZQWmtChan6XifU1CFd"
               let params =  "&staff_id=" + "\(self.staffId)" + "&admin_id=" + "\(self.adminId)"
               
               let url = String(apiUrl + params)
               getDevice(url)
        calculationForDateAndDay()
        //callGetStaffpunchinfoAPI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: .myNotificationKey, object: nil)
        //fourDigitNo
        NotificationCenter.default.addObserver(self, selector: #selector(self.fourDigitReceived(_:)), name: .fourDigitPin, object: nil)

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .myNotificationKey, object: nil)
        NotificationCenter.default.removeObserver(self, name: .fourDigitPin, object: nil)
    }
    
    @objc func fourDigitReceived(_ notification: Notification){
        guard let finalDigit = notification.userInfo?["finalDigit"] as? NSArray else { return }
        let pinCode = finalDigit.map {"\($0)"}.reduce("") { $0 + $1 }

       // if pinCode == self.loginData[0].digit_pin {
            // api call
        calculationForDateAndDay()
        if UserDefaults.standard.string(forKey: "userType") == "Staff"{
        callAddStaff(staffID: self.loginData[0].staff_id, adminID: self.loginData[0].admin_id, punchTime: self.currentTime, punchType: UserDefaults.standard.string(forKey: "punch_type") ?? "", punchDate: self.currnetDate, longitude: self.longitude, latitude: self.latitude, address: "\(self.city),\(self.country)",digitPin: pinCode)
        }else{
            callAddStaff(staffID: "", adminID: self.adminloginModel[0].user_id, punchTime: self.currentTime, punchType: UserDefaults.standard.string(forKey: "punch_type") ?? "", punchDate: self.currnetDate, longitude: self.longitude, latitude: self.latitude, address: "\(self.city),\(self.country)",digitPin: pinCode)
        }
//        }else{
//            //show alert
//            showAlertforInvalidQRCode(message: "Invalid Pin Code")
//        }
    }
    
    @objc func notificationReceived(_ notification: Notification) {
        guard let qrCode = notification.userInfo?["qrCode"] as? String else { return }
        if qrCode == self.loginData[0].digit_pin {
            // api call
            calculationForDateAndDay()
             if UserDefaults.standard.string(forKey: "userType") == "Staff"{
           callAddStaff(staffID: self.loginData[0].staff_id, adminID: self.loginData[0].admin_id, punchTime: self.currentTime, punchType: UserDefaults.standard.string(forKey: "punch_type") ?? "", punchDate: self.currnetDate, longitude: self.longitude, latitude: self.latitude, address: "\(self.city),\(self.country)",digitPin: qrCode)
             }else{
                 callAddStaff(staffID: "", adminID: self.adminloginModel[0].user_id, punchTime: self.currentTime, punchType: UserDefaults.standard.string(forKey: "punch_type") ?? "", punchDate: self.currnetDate, longitude: self.longitude, latitude: self.latitude, address: "\(self.city),\(self.country)",digitPin: qrCode)
            }
        }else{
            //show alert
            showAlertforInvalidQRCode(message: "Invalid QR Code")
        }
       
    }
    
    @objc func goToScannerPage(){
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
        destination.modalPresentationStyle = .fullScreen
        self.present(destination, animated: true, completion: nil)
    }
    
    @objc func goToPinPage(){
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "PinViewController") as! PinViewController
       destination.modalPresentationStyle = .custom
       self.present(destination, animated: true, completion: nil)
    }

   

    
    // MARK: - IBAction BUtotns
    @IBAction func backBtnPressed(_ sender: UIButton) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    @objc func signin(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AdminLoginController") as! AdminLoginController
            destination.loginData = self.loginData
            destination.deviceData = self.deviceData
            destination.modalPresentationStyle = .fullScreen
           
            self.present(destination, animated: true)
    }
    @objc func logOutAction(){
       // self.dismiss(animated: true) {
            let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            UserDefaults.standard.removeObject(forKey: "isLogin")
            UserDefaults.standard.removeObject(forKey: "KEY")
            UserDefaults.standard.removeObject(forKey: "KEY1")
            UserDefaults.standard.removeObject(forKey: "punch_type")
            destination.modalPresentationStyle = .fullScreen
            self.present(destination, animated: true)
       // }
    }
   
}

extension CheckInViewController {
    func showAlertforInvalidQRCode(message : String){
        let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel) { (_) in
           
        }
        alertController.addAction(okAction)
//        if let topController = UIApplication.topViewController() {
//            topController.present(alertController, animated: true, completion: nil)
//        }
        self.present(alertController, animated: true, completion: nil)
        
    }
}


// API CALLS
extension CheckInViewController{
    func callGetStaffpunchinfoAPI() {
        ///https://api.terraapp.net/data/GetStaffpunchinfo?key=GREIyYPzs7eq0e&salt=sIgTZQWmtChan6XifU1CFd&staff_id=31
        self.activityIndicator.startAnimating()
        let apiUrl = "https://api.timeconnect.net/data/GetStaffpunchinfo?key=GREIyYPzs7eq0e&salt=sIgTZQWmtChan6XifU1CFd"
        let params =  "&staff_id=" + "\(self.loginData[0].staff_id)" + "&punch_date=" + "\(self.currnetDate)"
        let url = String(apiUrl + params)
        
        if Connection.isConnectedToInternet(){
            Alamofire.request(url,
                              method: .get,
                              parameters: nil)
                .validate().responseJSON { (response) in
                    guard response.result.isSuccess else {
                        self.activityIndicator.stopAnimating()
                        return
                    }
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    let results = resposeJSON["status"] as? Bool ?? false
                    var punch_type = ""
                    if results {
                        if let dataArray = resposeJSON["data"]{
                            let data = dataArray as? NSArray ?? NSArray()
                            let staffPunchInfo = data[data.count - 1] as? NSDictionary ?? NSDictionary()
                            punch_type = staffPunchInfo["punch_type"] as? String ?? ""
                            let punch_time = staffPunchInfo["punch_time"] as? String ?? ""
                            
                            if data.count > 1{
                                let punchInfo = data[data.count - 2] as? NSDictionary ?? NSDictionary()
                                let type = punchInfo["punch_type"] as? String ?? ""
                                let time = punchInfo["punch_time"] as? String ?? ""
                                
                                if type == "OUT"{
                                    self.checkOutTimeLabel.text = "Check-Out Time  : " + "\(time)"
                                    self.checkInTimeLabel.text = "Check-In Time  : " + "\(punch_time)"
                                    
                                }else{
                                    self.checkInTimeLabel.text = "Check-In Time  : " + "\(time)"
                                    self.checkOutTimeLabel.text = "Check-Out Time  : " + "\(punch_time)"
                                }
                                
                                
                            }else{
                                if punch_type == "OUT"{
                                    self.checkOutTimeLabel.text = "Check-Out Time  : " + "\(punch_time)"
                                    
                                }else{
                                    self.checkInTimeLabel.text = "Check-In Time  : " + "\(punch_time)"
                                }
                            }
                            
                            
                            
                            
                        }else{
                             punch_type = resposeJSON["punch_type"] as? String ?? ""
                        }
                        
                        if punch_type == "OUT"{
                            UserDefaults.standard.set("IN", forKey: "punch_type")
                        }else{
                          UserDefaults.standard.set("OUT", forKey: "punch_type")
                        }
                        
                    }
            }
        }
        
    }
    
    func callAddStaff(staffID : String, adminID : String, punchTime : String, punchType : String, punchDate : String,longitude : String, latitude : String, address: String, digitPin: String){
        
        print("punchType\(punchType)")
        let bodyParams = [
                  "key"           : "GREIyYPzs7eq0e",
                  "salt"          : "sIgTZQWmtChan6XifU1CFd",
                 // "staff_id"      : staffID,
                  "admin_id"      : adminID,
                  "punch_time"    : punchTime,
                  //"punch_type"    : punchType,
                  "punch_date"    : punchDate,
                  "device_type"   : "MOB",
                  "longitude"     : longitude,
                  "latitude"      : latitude,
              "address_info"      : address,
              "digit_pin"         : digitPin
        ] as [String : String]
        
        self.activityIndicator.startAnimating()
        
        Alamofire.request("https://api.timeconnect.net/postdata/addpunch", method: .post, parameters: bodyParams).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
            //self.showAlertforInvalidQRCode(message: resposeJSON["message"] as? String ?? "Punch Added Successfully")
           // self.calculationForDateAndDay()
            if resposeJSON["status"] as? Bool ?? false == false{
                AudioServicesPlaySystemSound(1209)
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                self.showAlertforInvalidQRCode(message: "Invalid Pin Code")
            }
           else if resposeJSON["last_entry"] as? String ?? "" == ""{
            let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InAlertViewControler") as! InAlertViewControler
              //  destination.modalPresentationStyle = .fullScreen
           // destination.staffname = self.loginData[0].staff_name
            destination.staffname = resposeJSON["staff_name"] as? String ?? ""
            destination.last_entry = resposeJSON["last_entry"] as? String ?? ""
            destination.last_status = resposeJSON["last_status"] as? String ?? ""
            destination.punch_status = resposeJSON["punch_status"] as? String ?? ""
            destination.punch_time = resposeJSON["punch_time"] as? String ?? ""
            self.present(destination, animated: true)
           // self.callGetStaffpunchinfoAPI()
            
            }else{
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OutAlertViewControler") as! OutAlertViewControler
                             //  destination.modalPresentationStyle = .fullScreen
                           destination.staffname = resposeJSON["staff_name"] as? String ?? ""
                           destination.last_entry = resposeJSON["last_entry"] as? String ?? ""
                           destination.last_status = resposeJSON["last_status"] as? String ?? ""
                           destination.punch_status = resposeJSON["punch_status"] as? String ?? ""
                           destination.punch_time = resposeJSON["punch_time"] as? String ?? ""
                           self.present(destination, animated: true)
            }

    }
}
}

//Location
extension CheckInViewController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        _ = (location?.coordinate)!
        let currentlocation = CLLocation(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        
        fetchCityAndCountry(from: currentlocation) { city, country,locality,area,error  in
            guard let city = city,
                let country = country,
                let locality = locality,
                let area = area,
                error == nil else { return }
            print("\(city) + \(country) + \(locality) + \(area)" )
            self.longitude = String(location!.coordinate.longitude)
            self.latitude = String(location!.coordinate.latitude)
            self.city = city + "," + locality  + "," + area 
            self.country = country
            self.locationManager.stopUpdatingLocation()
        }
        
        print("latitude\(location!.coordinate.latitude)...\(location!.coordinate.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?,_ locality: String?,_ area: String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       placemarks?.first?.postalCode,
                       placemarks?.first?.administrativeArea,
                       error)
        }
    }
}
struct DeviceDataModel : Codable {
    var enable_scanner = ""
    var rear_camera = ""
    var device_sleep = ""
}
