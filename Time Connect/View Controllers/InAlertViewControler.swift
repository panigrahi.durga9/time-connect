//
//  InAlertViewControler.swift
//  Time Connect
//
//  Created by Sujit MacBook on 17/12/20.
//  Copyright © 2020 TimeConnect. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
class InAlertViewControler: UIViewController{
    @IBOutlet weak var namelabel: UILabel!
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var okbutton: UIButton!
    @IBOutlet weak var intimelabel: UILabel!
    var staffname = ""
    var punch_time = ""
    var punch_status = ""
    var last_entry = ""
    var last_status = ""
    override func viewDidLoad() {
        AudioServicesPlaySystemSound(1209)
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        popUpView.layer.cornerRadius = 4.0
        popUpView.layer.shadowColor = UIColor.black.cgColor
        popUpView.layer.shadowOpacity = 0.5
        popUpView.layer.shadowOffset = .zero
        popUpView.layer.shadowRadius = 5
       // namelabel.backgroundColor = .green
        namelabel.textColor = .white
        namelabel.text = staffname
       
        self.timeConversion12(time24: punch_time)
        
//
//      let dateFormatter = DateFormatter()
//       dateFormatter.dateFormat = "HH:mm:ss"
//       var dateFromStr = dateFormatter.date(from: punch_time)!
//       // var dateFromStr = dateFormatter.date(from: punch_time)!
//        dateFormatter.dateFormat = "hh:mm:ss a"
//        dateFormatter.amSymbol = "am"
//        dateFormatter.pmSymbol = "Pm"
//        dateFormatter.dateFormat = "a"
//        var timeFromDate = dateFormatter.string(from: dateFromStr)
//        print(timeFromDate)
       // timelabel.text = punch_time
//
//            intimelabel.text = punch_status
//            timelabel.text = punch_time
//            timelabel.textColor = .green
//            intimelabel.textColor = .green
//            namelabel.backgroundColor = .green
        
        okbutton.addTarget(self, action: #selector(dismisss), for: .touchUpInside)
    }
    func timeConversion12(time24: String) -> String {
               let dateAsString = time24
               let df = DateFormatter()
               df.dateFormat = "HH:mm:ss"
               let date = df.date(from: dateAsString)
               df.dateFormat = "hh:mm a"
               let time12 = df.string(from: date!)
               print(time12)

        intimelabel.text = punch_status
        timelabel.text = time12
        timelabel.textColor = .green
        intimelabel.textColor = .green
        namelabel.backgroundColor = .green
               return time12
           }
    @objc func dismisss(){
         self.dismiss(animated: true, completion: nil)
    }
}
