//
//  LoginViewController.swift
//  TERRA
//
//  Created by chhavi  kaushik on 27/03/19.
//  Copyright © 2019 iquincesoft. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView


class LoginViewController: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var signinButton: UIButton!
    var loginModel = [LoginModel]()
    var adminloginModel = [AdminLoginModel]()
    @IBOutlet weak var signInButton: CustomButton!
    var activityIndicator: NVActivityIndicatorView!

    fileprivate func setUpView() {
        
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .ballSpinFadeLoader, color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), padding: .zero)
        activityIndicator.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        self.view.addSubview(activityIndicator)
        
        signInButton.cornorRadius = 10
        self.signInButton.clipsToBounds = true
        self.signInButton.backgroundColor = #colorLiteral(red: 0.1646597683, green: 0.3915407658, blue: 0.6790658832, alpha: 1)
        signInButton.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        signInButton.borderWidth1 = 1
        signInButton.shadowColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        signInButton.shadowOffset = CGSize(width: 0, height: 3)
        signInButton.shadowOpacity = 0.6
        signInButton.layer.shadowRadius = 10.0
        self.signInButton.layer.masksToBounds = false
        self.signInButton.setTitleColor(.white, for: .normal)

        
        self.tfUsername.setTextFieldImage(image: FAType.FAUser, direction: .left)
        self.tfUsername.keyboardType = .emailAddress
        self.tfPassword.setTextFieldImage(image: FAType.FALock, direction: .left)
        self.tfPassword.isSecureTextEntry = true
        
        self.tfUsername.setPlaceholder(placeholder: "Enter Your Mail / Phone No.", color: UIColor.white.withAlphaComponent(0.6))
        self.tfPassword.setPlaceholder(placeholder: "Enter Your Password", color: UIColor.white.withAlphaComponent(0.6))
        
        self.tfUsername.textColor = .white
        self.tfPassword.textColor = .white
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tfUsername.addButtomBorder()
        self.tfPassword.addButtomBorder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        tfUsername.alpha = 0
        tfPassword.alpha = 0
        signInButton.alpha = 0
        
        UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .transitionCurlDown, animations: {
            self.tfUsername.alpha = 1
        }) { _ in
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
                self.tfPassword.alpha = 1
                
            })

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
                    self.signInButton.alpha = 1
                    
                })
            }
        }
    }
    // MARK: - Ibaction buttons

    
    @IBAction func btnLoginPressed(_ sender: CustomButton) {
        
        if Common.sharedInstance.isFieldBlank(textfield: tfUsername.text ?? "") || Common.sharedInstance.isFieldBlank(textfield: tfPassword.text ?? "") {
            
            Common.sharedInstance.showUserMessage(UserMessaeg: "Email/Password field should not be blank")
        }
        else if !Common.sharedInstance.isValidPassword(password: tfPassword.text ?? ""){
            Common.sharedInstance.showUserMessage(UserMessaeg:"Please enter valid Password !")
        }else if Common.sharedInstance.isValidEmail(email: tfUsername.text ?? "") || Common.sharedInstance.isValidPhoneNumber(phNo: tfUsername.text ?? ""){
            
            //https://api.terraapp.net/data/GetStaffLogin?key=GREIyYPzs7eq0e&salt=sIgTZQWmtChan6XifU1CFd&email_id=durga@muvi.com&password=123456
            let apiUrl = "https://api.timeconnect.net/data/GetStaffLogin?key=GREIyYPzs7eq0e&salt=sIgTZQWmtChan6XifU1CFd"
            let params =  "&email_id=" + "\(tfUsername.text ?? "")" + "&password=" + "\(tfPassword.text ?? "")"
            let url = String(apiUrl + params)
            CallLoginApi(url)
        }else{
              Common.sharedInstance.showUserMessage(UserMessaeg:"Please enter valid Email / Phone number!")
        }
     
    }
    
    //MARK: - TextField Delegaate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 2 {
           tfPassword.isSecureTextEntry = true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        return true;
    }
   
    fileprivate func goToCheckInPage() {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
        destination.loginData = self.loginModel
        destination.adminloginModel = self.adminloginModel
        destination.modalPresentationStyle = .custom
        self.present(destination, animated: true, completion: nil)
    }
    
    fileprivate func showAlertwhileApiFails(_ resposeJSON: NSDictionary) {
        let alertController = UIAlertController(title: "Alert!", message: resposeJSON["message"] as? String ?? "Staff not Matching", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel) { (_) in
            
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func CallLoginApi(_ url: String)  {
        self.loginModel.removeAll()
        self.adminloginModel.removeAll()
        if Connection.isConnectedToInternet(){
         self.activityIndicator.startAnimating()
        Alamofire.request(url,
                          method: .get,
                          parameters: nil)
                .validate().responseJSON { (response) in
                    guard response.result.isSuccess else {
                        self.activityIndicator.stopAnimating()
                        return
                    }
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    let type = resposeJSON["type"] as? String ?? ""
                     UserDefaults.standard.set(type, forKey: "userType")
                    if type == "Staff"{
                    let results = resposeJSON["status"] as? Bool ?? false
                    
                    if results {
                        UserDefaults.standard.set(true, forKey: "isLogin")
                        let data = resposeJSON["data"] as? NSArray ?? NSArray()
                        let loginData = data[0] as? NSDictionary ?? NSDictionary()
                        let staff_name = loginData["staff_name"] as? String ?? ""
                        let digit_pin = loginData["digit_pin"] as? String ?? ""
                        let empl_id = loginData["empl_id"] as? String ?? ""
                        let staff_id = loginData["staff_id"] as? String ?? ""
                        let admin_id = loginData["admin_id"] as? String ?? ""
                        
                        let loginModel = LoginModel(staff_name: staff_name, digit_pin: digit_pin, empl_id: empl_id, staff_id: staff_id, admin_id: admin_id)
                        
                        self.loginModel.append(loginModel)
                        // save it easily!
                        UserSessionManager.shared.loginDetails = self.loginModel
                        self.goToCheckInPage()
                        
                    }else{
                        self.showAlertwhileApiFails(resposeJSON)
                    }
                    }else{
                        let results = resposeJSON["status"] as? Bool ?? false
                        
                        if results {
                            UserDefaults.standard.set(true, forKey: "isLogin")
                            let data = resposeJSON["data"] as? NSArray ?? NSArray()
                            let adminloginData = data[0] as? NSDictionary ?? NSDictionary()
                            let user_name = adminloginData["user_name"] as? String ?? ""
                            let user_id = adminloginData["user_id"] as? String ?? ""
                            let user_email = adminloginData["user_email"] as? String ?? ""
                            let company_name = adminloginData["company_name"] as? String ?? ""
                            let enable_scanner = adminloginData["enable_scanner"] as? String ?? ""
                            let rear_camera = adminloginData["rear_camera"] as? String ?? ""
                            let device_sleep = adminloginData["device_sleep"] as? String ?? ""
                            
                            let loginModel1 = AdminLoginModel(user_name: user_name, user_id:user_id, user_email: user_email, company_name: company_name, enable_scanner: enable_scanner, rear_camera: rear_camera, device_sleep: device_sleep)
                            
                            self.adminloginModel.append(loginModel1)
                            // save it easily!
                            UserSessionManager.shared.loginDetailss = self.adminloginModel
                            self.goToCheckInPage()
                            
                        }else{
                            self.showAlertwhileApiFails(resposeJSON)
                        }
                    }
            }
        }
        
    }
    
}


struct LoginModel : Codable {
    var staff_name = ""
    var digit_pin = ""
    var empl_id = ""
    var staff_id = ""
    var admin_id = ""
}


