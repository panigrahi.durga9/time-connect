//
//  OutAlertViewControler.swift
//  Time Connect
//
//  Created by Sujit MacBook on 17/12/20.
//  Copyright © 2020 TimeConnect. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class OutAlertViewControler: UIViewController {
    @IBOutlet weak var namelabel: UILabel!
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var okbutton: UIButton!
    @IBOutlet weak var intimelabel: UILabel!
    
    @IBOutlet weak var timediffer: UILabel!
    @IBOutlet weak var outtime: UILabel!
    @IBOutlet weak var outlabel: UILabel!
    @IBOutlet weak var inlable: UILabel!
    @IBOutlet weak var intime: UILabel!
    var staffname = ""
    var punch_time = ""
    var punch_status = ""
    var last_entry = ""
    var last_status = ""
    var punchOutTime = ""
    var punchInTime = ""
    var lastEntryTime = ""
    override func viewDidLoad() {
        AudioServicesPlaySystemSound(1209)
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        popUpView.layer.cornerRadius = 4.0
        popUpView.layer.shadowColor = UIColor.black.cgColor
        popUpView.layer.shadowOpacity = 0.5
        popUpView.layer.shadowOffset = .zero
        popUpView.layer.shadowRadius = 5
        
        self.timeConversion12(time24: punch_time)
        let dateAsString = last_entry
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        let date = df.date(from: dateAsString)
        df.dateFormat = "hh:mm a"
        intime.text = df.string(from: date!)
        namelabel.textColor = .white
        namelabel.text = staffname
        intimelabel.text = punch_status
        timelabel.text = self.punchOutTime
        timelabel.textColor = .red
        intimelabel.textColor = .red
        namelabel.backgroundColor = .red
        //intime.text = last_entry
        outtime.text = self.punchOutTime
      
        timediffer.text = findDateDiff(time1Str: last_entry, time2Str: punch_time)
      okbutton.addTarget(self, action: #selector(dismisss), for: .touchUpInside)
        }
        @objc func dismisss(){
             self.dismiss(animated: true, completion: nil)
        }
    func timeConversion12(time24: String) -> String {
           let dateAsString = time24
           let df = DateFormatter()
           df.dateFormat = "HH:mm:ss"
           let date = df.date(from: dateAsString)
           df.dateFormat = "hh:mm a"
          self.punchOutTime = df.string(from: date!)
           print(self.punchOutTime)
           return self.punchOutTime
       }
  
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "HH:mm:ss"

        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }

        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        return "\(intervalInt < 0 ? "-" : "+") \(Int(hour)) Hrs \(Int(minute)) Mins"
    }
}
