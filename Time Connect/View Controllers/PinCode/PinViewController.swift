//
//  PinViewController.swift
//  Time Connect
//
//  Created by Mona on 27/11/20.
//  Copyright © 2020 TimeConnect. All rights reserved.
//

import UIKit

class PinViewController: UIViewController {
    
    @IBOutlet weak var pinView: PinView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pinView.delegate = self
        

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
extension PinViewController : PinViewDelegate{
    func handle(_ event: PinEvent) {
        switch event {
        case .inputChanged(let currentDigits):
            print(currentDigits)
        case .inputCompleted(let finalDigit):
            print(finalDigit)
            
           
            dismiss(animated: true) {
                let userInfo = [ "finalDigit" : finalDigit ]
                
                NotificationCenter.default.post(name: .fourDigitPin, object: nil, userInfo: userInfo)
                
            }
        case .backspacePressed,.optionPressed :
            break
        }
    }
    
    var clearInputOnCompleted: Bool { return false }
    
    
}
extension PinViewController: PinViewConfigurationProvider {

    var pinInputViewConfiguration: PinInputViewConfiguration {
        return PinInputViewConfiguration.standard
    }
    var pinOutputViewConfiguration: PinOutputViewConfiguration {
        return PinOutputViewConfiguration.standard
    }
}
