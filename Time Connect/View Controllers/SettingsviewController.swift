//
//  SettingsviewController.swift
//  Time Connect
//
//  Created by Sujit MacBook on 15/12/20.
//  Copyright © 2020 TimeConnect. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import Alamofire

class SettingsviewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var backButton: CustomButton!
    @IBOutlet weak var tableView: UITableView!
    var adminloginData = [AdminLoginModel]()
    var deviceData = [DeviceDataModel]()
    var loginData = [LoginModel]()
    var scannerEnabled = "0"
    var deviceSleepenabled = "0"
    var rearCameraEnabled = "0"
    var activityIndicator: NVActivityIndicatorView!
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 50
        }else if indexPath.section == 1{
            return 60
    }else if indexPath.section == 2{
        return 50
    }else if indexPath.section == 3{
        return 50
        }
        else if indexPath.section == 4{
        return 50
        }
        return 20
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
        return 3
        }else if section == 1{
           return 1
        }else if section == 2{
           return 2
        }else if section == 3{
           return 1
        }else if section == 4{
           return 1
        }
        return 4
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell: UITableViewCell = UITableViewCell()
        if indexPath.section == 0{
            if indexPath.row == 0{
            let cell: ProfileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
            cell.label.text = "Account"
            cell.label.textColor = .black
                cell.profilelable.text = self.adminloginData[0].user_email
            cell.profilelable.textColor = .blue
            }
            else if indexPath.row == 1{
                let cell: ProfileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
                cell.label.text = "Company"
                cell.label.textColor = .black
                cell.profilelable.text = self.adminloginData[0].company_name
                cell.profilelable.textColor = .blue
            }
            else if indexPath.row == 2{
                let cell: ProfileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
                cell.label.text = "Device"
                cell.label.textColor = .black
                cell.profilelable.text = "Iphone8"
                cell.profilelable.textColor = .blue
            }
       return cell
        }else if indexPath.section == 1{
            let cell: LogoutCell = tableView.dequeueReusableCell(withIdentifier: "LogoutCell", for: indexPath) as! LogoutCell
            cell.logoutButton.addTarget(self, action: #selector(logoutBttonPressed), for: .touchUpInside)
            return cell
        }
        else if indexPath.section == 2{
            if indexPath.row == 0{
            let cell: DetailsCell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath) as! DetailsCell
            cell.detaillabel.text = "Enable Scanner"
            cell.detaillabel.textColor = .black
                cell.switchbutton.addTarget(self, action: #selector(sacnnerEnable(_:)), for: .valueChanged)
                if self.deviceData.count > 0{
                if self.deviceData[0].enable_scanner == "1"{
                    cell.switchbutton.isOn = true
                }else{
                    cell.switchbutton.isOn = false
                }
                }
            }else if indexPath.row == 1{
             let cell: DetailsCell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath) as! DetailsCell
             cell.detaillabel.text = "Use Rear Camera"
             cell.detaillabel.textColor = .black
            cell.switchbutton.addTarget(self, action: #selector(rearCamera(_:)), for: .valueChanged)
                if self.deviceData.count > 0{
                if self.deviceData[0].rear_camera == "1"{
                    cell.switchbutton.isOn = true
                }else{
                     cell.switchbutton.isOn = false
                }
                }
             }
            return cell
        }else if indexPath.section == 3{
         
         let cell: DetailsCell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath) as! DetailsCell
         cell.detaillabel.text = "Allow Device Sleep"
         cell.detaillabel.textColor = .black
            cell.switchbutton.addTarget(self, action: #selector(deviceSleep(_:)), for: .valueChanged)
            if self.deviceData.count > 0{
            if self.deviceData[0].device_sleep == "1"{
                cell.switchbutton.isOn = true
            }else{
                cell.switchbutton.isOn = false
            }
            }
         return cell
        }else if indexPath.section == 4{
        let cell: ProfileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        cell.label.text = "Version"
        cell.label.textColor = .black
        cell.profilelable.text = "1.6.1090"
        cell.profilelable.textColor = .black
        return cell
        }
        return cell
    }
   
    func modifyDeviceData(adminID : String, enableScanner : String, rearCamera : String, deviceSleep : String){
        
        //print("punchType\(punchType)")
        let bodyParams = [
            "key"           : "GREIyYPzs7eq0e",
            "salt"          : "sIgTZQWmtChan6XifU1CFd",
           // "staff_id"      : staffID,
            "admin_id"      : adminID,
            "enable_scanner"    : enableScanner,
            "rear_camera"    : rearCamera,
            "device_sleep"   : deviceSleep
            ] as [String : String]
        
        self.activityIndicator.startAnimating()
        
        Alamofire.request("https://api.timeconnect.net/postdata/setdevice", method: .post, parameters: bodyParams).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
             let alertController = UIAlertController(title: "Alert!", message: resposeJSON["message"] as? String ?? "Setting Applied Successfully", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .cancel) { (_) in
                       
                    }
                    alertController.addAction(okAction)
           
                    self.present(alertController, animated: true, completion: nil)
          //  self.showAlertforInvalidQRCode(message: resposeJSON["message"] as? String ?? "Punch Added Successfully")
            // self.calculationForDateAndDay()
           // if resposeJSON["last_entry"] as? String ?? "" == ""else
            
        }
    }
    @objc func sacnnerEnable(_ sender: UISwitch!){
        if (sender.isOn) == true{
            self.scannerEnabled = "1"
        }else{
            self.scannerEnabled = "0"
        }
        
        if self.adminloginData.count > 0{
        modifyDeviceData(adminID: self.adminloginData[0].user_id, enableScanner: self.scannerEnabled, rearCamera: self.rearCameraEnabled , deviceSleep: self.deviceSleepenabled )
        }
    }
    @objc func rearCamera(_ sender: UISwitch!){
           if (sender.isOn) == true{
               self.rearCameraEnabled = "1"
           }else{
               self.rearCameraEnabled = "0"
           }
        if self.adminloginData.count > 0{
           modifyDeviceData(adminID: self.adminloginData[0].user_id, enableScanner: self.scannerEnabled, rearCamera: self.rearCameraEnabled , deviceSleep: self.deviceSleepenabled )
        }
       }
    @objc func deviceSleep(_ sender: UISwitch!){
              if (sender.isOn) == true{
                  self.deviceSleepenabled = "1"
                  UIApplication.shared.isIdleTimerDisabled = false
              }else{
                  self.deviceSleepenabled = "0"
              }
        if self.adminloginData.count > 0{
              modifyDeviceData(adminID: self.adminloginData[0].user_id, enableScanner: self.scannerEnabled, rearCamera: self.rearCameraEnabled , deviceSleep: self.deviceSleepenabled )
        }
          }
    override func viewDidLoad() {
        
        self.scannerEnabled = self.deviceData[0].enable_scanner
        self.rearCameraEnabled = self.deviceData[0].rear_camera
        self.deviceSleepenabled = self.deviceData[0].device_sleep
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .ballSpinFadeLoader, color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), padding: .zero)
        activityIndicator.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        self.view.addSubview(activityIndicator)
    self.tableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
    self.tableView.register(UINib(nibName: "DetailsCell", bundle: nil), forCellReuseIdentifier: "DetailsCell")
    self.tableView.register(UINib(nibName: "LogoutCell", bundle: nil), forCellReuseIdentifier: "LogoutCell")
    self.backButton.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
    }
    @objc func logoutBttonPressed(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AdminLoginController") as! AdminLoginController
        UserDefaults.standard.removeObject(forKey: "isLogin")
        UserDefaults.standard.removeObject(forKey: "KEY1")
        UserDefaults.standard.removeObject(forKey: "punch_type")
        destination.modalPresentationStyle = .fullScreen
        self.present(destination, animated: true)
    }
    @objc func backPressed(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
            destination.loginData = UserSessionManager.shared.loginDetails
            destination.adminloginModel = UserSessionManager.shared.loginDetailss
            destination.modalPresentationStyle = .fullScreen
            self.present(destination, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        headerView.addSubview(headerLabel)
         headerView.backgroundColor = .clear
//        headerLabel.anchor(headerView.topAnchor, left: headerView.leftAnchor, bottom: headerView.bottomAnchor, right:  nil, topConstant: 0, leftConstant: 4, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        headerLabel.textColor = UIColor.textColor()
//        headerLabel.font = UIFont.lblHeader()
//        headerLabel.font = headerLabel.font.withSize(17)
        
        if section == 2
        {
            
            headerLabel.text =  "Card Scanner"
            
        }
        else if section == 3 || section == 1
               {
                   
                   headerLabel.text =  ""
                   
               }
        else if section == 4
        {
            
            headerLabel.text =  "About TimeStation"
            
        }
        //        else if section == 2 && relatedcontent_array_Aod.count > 0
        //        {
        //            headerLabel.text = UserDefaults.standard.string(forKey: "related_audios") ?? "RELATED AUDIOS"
        //
        //        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // if section == 1 || section == 2 {
        if  section == 2 || section == 3 || section == 4{
            return 50.0
        }else if section == 1{
            return 20.0
        }
        return 0.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let headerLabel = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.width - 10, height: 25))
        headerLabel.text = "Allow Screen to turn off after a period of inactivity"
        headerLabel.font = UIFont(name: "", size: 5)
        footerView.addSubview(headerLabel)
       
         footerView.backgroundColor = .clear
        return footerView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 3{
          return 50
        }
        return 0
    }

}
