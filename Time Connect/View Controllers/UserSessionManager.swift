//
//  UserSessionManager.swift
//  TERRA
//
//  Created by Mona on 20/11/20.
//  Copyright © 2020 iquincesoft. All rights reserved.
//

import Foundation

class UserSessionManager
{
    // MARK:- Properties

    public static var shared = UserSessionManager()
    
    // MARK:- Init

    private init(){}

    var loginDetails: [LoginModel]
    {
        get
        {

            if let data = UserDefaults.standard.value(forKey:"KEY") as? Data {
                let decodedItems = (try? PropertyListDecoder().decode([LoginModel].self, from: data)) ?? []

                return decodedItems
            }
            return []
        }
        set
        {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey:"KEY")
            UserDefaults.standard.synchronize()

        }
    }
    var loginDetailss: [AdminLoginModel]
    {
        get
        {

            if let data = UserDefaults.standard.value(forKey:"KEY1") as? Data {
                let decodedItems = (try? PropertyListDecoder().decode([AdminLoginModel].self, from: data)) ?? []

                return decodedItems
            }
            return []
        }
        set
        {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey:"KEY1")
            UserDefaults.standard.synchronize()

        }
    }

    
}
